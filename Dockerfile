FROM debian
  
# Install.
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN \
  apt-get update && \
  apt-get -y upgrade && \
  DEBIAN_FRONTEND=noninteractive apt-get install -q -y postfix && \
  apt-get install -y php-redis nginx php php-pgsql  php-gd php-curl php-fpm php-cgi php-cli php-zip  && \
  apt-get install -y supervisor  postgresql-client postgresql-client-common postgresql-contrib && \
  apt-get install -y php-memcached php-soap php-ctype  php-zip php-simplexml  php-dom php-xml php-json php-intl php-fpm php-common php-mbstring php-xmlrpc php-soap php-gd php-xml php-intl php-mysql php-cli php-ldap php-zip php-curl libpq-dev  rsyslog  && \
  apt-get install -y net-tools memcached curl git htop man unzip vim wget
RUN apt-get install -y --no-install-recommends --no-install-suggests supervisor

#RUN useradd -ms /bin/bash elearning
#RUN mkdir /home/elearning/moodle
#RUN chown -R elearning:elearning /home/elearning/moodle

COPY cronmoodle /etc/cron.d/cronmoodle
RUN chmod 644 /etc/cron.d/cronmoodle
RUN crontab /etc/cron.d/cronmoodle

COPY  . /var/www/html/
RUN chown www-data:www-data /var/www
RUN chown -R www-data:www-data /var/www/*
RUN mkdir /var/www/html/localcache
RUN chown www-data:www-data /var/www/html/localcache

RUN mkdir -p /run/php
COPY php.ini /etc/php/7.3/fpm/php.ini
COPY default /etc/nginx/sites-enabled/default
##COPY www.conf /etc/php/7.2/fpm/pool.d/www.conf
COPY start_php-fpm7.sh /start_php-fpm7.sh
CMD chown www-data:www-data /var/www
CMD chown -R www-data:www-data /var/www/*
RUN chmod +x /start_php-fpm7.sh
CMD ["/usr/sbin/php-fpm7.3", "-F"]
RUN ./start_php-fpm7.sh
RUN phpenmod opcache
COPY supervisor.conf /etc/supervisor/conf.d/supervisor.conf

CMD ["/usr/bin/supervisord"]
RUN rm -rf /var/www/html/cronmoodle
RUN rm -rf /var/www/html/start_php-fpm7.sh
RUN rm -rf /var/www/html/index.html
RUN rm -rf /var/www/html/Dockerfile
RUN rm -rf /var/www/html/default
RUN rm -rf /var/www/html/php.ini
RUN rm -rf /var/www/html/start_nginx.sh
RUN rm -rf /var/www/html/wrapper.sh
RUN rm -rf /var/www/html/www.conf
RUN rm -rf /var/www/html/supervisor.conf
